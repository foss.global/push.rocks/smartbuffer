/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartbuffer',
  version: '1.0.3',
  description: 'handle ArrayBufferLike structures'
}
